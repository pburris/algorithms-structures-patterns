# Algorithms, Data Structures and Design Patterns



# TODO:

I need to wait until I have most of the project coded until I really start digging into this file.

Also needed:

  0. Get a plan for how the testing structure and/or the lib/module builder would look like
  1. Some sort of build script or makefile in the bin folder
  2. Linting/Standards: Set up linters for the languages. Set up standards for the project
  3. Tests: I need to be able to run tests from the root folder, I should be able to easily run certain sections, languages, subsections, files, etc.
  4. Library: I want to be able to build a library/module/etc. for each language, based on flags
  5. Then I want to be able to build submodules based on flags (only trees, or only graphs and the A\* algorithm)
  10. Have each language's folders adhere to the best practices of that language
  50. Eventually add a cli tool to scaffold out new features
  100. Add new languages, get pull requests, make this better with community help, create awesome tool that doubles as a knowledge repository

### Languages Covered

  1. Python
  2. JavaScript
  3. Elixir
  4. Ruby
  5. Go

### Table of Contents

  1. Russian Peasants Algorithm


### Planned

#### Patterns

  * Creational Design Patterns
    * Abstract Factory
    * Builder
    * Dependency Injection
    * Factory Method
    * Lazy Initialization
    * Multiton
    * Object Pool
    * Prototype
    * Resource acquisition is initialization
    * Singleton
  * Structural Design Patterns
    * Adapter
    * Bridge
    * Composite
    * Decorator
    * Facade
    * Flyweight
    * Front Controller
    * Marker
    * Module
    * MVC
    * Private Class Data (Data Access Object)
    * Proxy
    * Twin
  * Behavioral Design Patterns
    * Blackboard
    * Chain of Responsibility
    * Command
    * Interpreter
    * Iterator
    * Mediator
    * Memento
    * Null Object
    * Observer
    * Servant
    * State
    * Strategy
    * Template Method
    * Visitor
  * Concurrency Design Patterns
    * Active Object
    * Balking
    * Binding Properties
    * Blockchain
    * Double-checked Locking
    * Event-based Async
    * Guarded Suspension
    * Join
    * Lock
    * Messaging Design Pattern
    * Monitor Object
    * Reactor
    * Read-Write Lock
    * Scheduler
    * Thread Pool
    * Thread-specific Storag

#### Data Structures and Algorithms

  * Data Structures
    * Lists
    * Arrays
    * Stacks/Queues
    * Trees
    * Graphs
    * Recursion
  * Basic Algorithms
    * Multiplication
    * Division
    * Square Root
    * Manipulating Lists, Arrays, Stacks and Queues
    * Asymptotic Notation and Analysis
    * Binary Trees
    * Greedy Algorithms
    * Divide and Conquer
    * Dynamic Programming
  * Search Algorithms
    * Linear Search
    * Binary Search
    * Breadth-first search
    * Depth-first Search
    * Interpolation Search
    * Hash Table
  * Sort Algorithms
    * Bubble Sort
    * Insertion Sort
    * Selection Sort
    * Merge Sort
    * Shell Sort
    * Quick Sort
