# Patterns

### Table of Contents

  * Creational Design Patterns
    * Builder
    * Dependency Injection
    * Factory
    * Lazy Initialization
    * Object Pool
    * Singleton
  * Structural Design Patterns
    * Adapter
    * Bridge
    * Decorator
    * Flyweight
    * Proxy
  * Behavioral Design Patterns
    * Chain of Responsibility
    * Command
    * Iterator
    * Observer
    * State


**LATER:**
  * Concurrency Design Patterns
    * Balking
    * Lock
    * Messaging Design Pattern
    * Reactor
    * Read-Write Lock
    * Scheduler
    * Thread Pool


### What is an algorithm?
