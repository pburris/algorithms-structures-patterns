import sys
from functools import reduce

def rntoi(numeral):
    numeral = numeral.lower()
    switch = {
        'i': 1,
        'v': 5,
        'x': 10,
        'l': 50,
        'c': 100,
        'd': 500,
    }

    try:
        return switch[numeral]
    except:
        return -1

def checkErrors(errList):
    def _checkErrors(rn):
        i = rntoi(rn)
        if i == -1:
            errList.append("invalid numeral {}".format(rn))
            return 0
        return i
    return _checkErrors

def rcompare(a, b):
    if a < b:
        return -a
    return a

def convert(string):
    errors = []

    # convert to decimal, check for errors
    nums = [checkErrors(errors)(s) for s in string]

    # compare the digits and check for sign change
    nums = [rcompare(a, b) for a,b in zip(nums, nums[1:] + [0])]

    # sum the digits
    total = reduce(lambda x,y: x+y, nums)

    return { "errors": errors, "total": total }


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: python main.py [numeral]")
    else:
        out = convert(sys.argv[1])
        if len(out['errors']) > 0:
            for e in out['errors']: print(e)
        else:
            print("{} ----> {}".format(sys.argv[1], out['total']))
