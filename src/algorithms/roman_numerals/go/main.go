package main

import (
	"errors"
	"fmt"
	"os"
	"strings"
)

func rntoi(numeral rune) (int, error) {
	switch numeral {
	case 'i':
		return 1, nil
	case 'v':
		return 5, nil
	case 'x':
		return 10, nil
	case 'l':
		return 50, nil
	case 'c':
		return 100, nil
	case 'd':
		return 500, nil
	case 'm':
		return 1000, nil
	default:
		return -1, errors.New(fmt.Sprintf("no such roman numeral: %c", numeral))
	}
}

func convert(in string) (int, error) {
	ttl := 0
	input := strings.ToLower(in)
	len := len(input)

	for index, r := range strings.ToLower(input) {
		first, err := rntoi(r)
		if err != nil {
			return -1, err
		}
		if !(index+1 >= len) {
			second, err := rntoi(([]rune(input))[index+1])
			if err != nil {
				return -1, err
			}
			if first < second {
				ttl = ttl - first
				continue
			}
		}
		ttl = ttl + first
	}

	return ttl, nil
}

func main() {
	args := os.Args

	if len(args) < 2 {
		fmt.Println("Wrong number of arguments")
	}

	i, err := convert(args[1])
	if err != nil {
		panic(err)
	}

	fmt.Printf("%d\n", i)
}
