// Roman Numberal to Integer
const rntoi = n => {
  const numeral = n.toLowerCase();

  switch (numeral) {
    case 'i':
      return 1;
    case 'v':
      return 5;
    case 'x':
      return 10;
    case 'l':
      return 50;
    case 'c':
      return 100;
    case 'd':
      return 500;
    case 'm':
      return 1000;
    default:
      return -1;
  }
};

// Check the numeral for errors
const checkErrors = errList => (rn, index) => {
  const i = rntoi(rn);
  if (i === -1) errList.push(`illegal numeral: ${rn} at position: ${index}`);
  return i === -1 ? 0 : i;
};

// Reduce the values into a single number
const reduceValues = (ttl, cur, index, src) => {
  if (index < src.length - 1) {
    if (cur < src[index + 1]) {
      return ttl - cur;
    }
  }
  return ttl + cur;
};

// Convert the string input into an integer
const convert = input => {
  const errors = [];
  const total = input
    .split('')
    .map(checkErrors(errors))
    .reduce(reduceValues, 0);

  return { errors, total };
};

// Main function
const main = () => {
  const args = process.argv;
  if (args.length < 3) {
    console.log('Not enough arguments');
  } else {
    const out = convert(args[2]);
    if (out.errors.length) {
      out.errors.forEach(e => console.log(e));
    } else {
      console.log(`${args[2]} converts to: ${out.total}`);
    }
  }
};

main();
