#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

const int MESSAGE_LIMIT = 256;

struct error_list {
  int    count;
  char** messages;
};
typedef struct error_list ErrorList;

ErrorList new_elist();
void       destroy_elist(ErrorList e);
void       push_elist(ErrorList* e, char c, int pos);
void       print_elist(ErrorList e);

int rntoi(char c, ErrorList* e, int pos, int report);
int convert(char* input, ErrorList* e);

// Entry
int main(int argc, char** argv) {
  if (argc < 2) {
    printf("Usage: roman [numerals]\n");
    return -1;
  }

  ErrorList e = new_elist();
  int out = convert(argv[1], &e);

  if (e.count > 0) {
    print_elist(e);
    destroy_elist(e);
    return -1;
  }

  printf("%s ----> %d\n", argv[1], out);
  destroy_elist(e);
  return 0;
}

// roman numeral to integer
int rntoi(char c, ErrorList* e, int pos, int report) {
	switch (c) {
	case 'i':
		return 1;
	case 'v':
		return 5;
	case 'x':
		return 10;
	case 'l':
		return 50;
	case 'c':
		return 100;
	case 'd':
		return 500;
	case 'm':
		return 1000;
	default: {
      if (report == 1) push_elist(e, c, pos);
      return 0;
    }
	}
}

// Convert a roman numeral string to integer
int convert(char* input, ErrorList *e) {
  int i = 0;
  char c;
  int total = 0;
  while (c = input[i], c != '\0') {
    c = tolower(c);
    int o = rntoi(c, e, i, 1);
    if (input[i+1] != '\0') {
      int o1 = rntoi(input[i+1], e, i, 0);
      if (o < o1) {
        total -= o;
      } else {
        total += o;
      }
    } else {
      total += o;
    }
    i++;
  }

  return total;
}

// create new error list
ErrorList new_elist() {
  ErrorList* e = (ErrorList*) malloc(sizeof(ErrorList*));
  e->count = 0;
  e->messages = malloc(sizeof(char*) * MESSAGE_LIMIT);
  return *e;
}

// Destroy error list
void destroy_elist(ErrorList e) {
  int i;
  for (i = 0; i < e.count; i++) {
    free(e.messages[i]);
  }
  free(e.messages);
}

// Add to the error list
void push_elist(ErrorList *e, char c, int pos) {
  char *msg = (char*) malloc(sizeof(char) * MESSAGE_LIMIT);
  sprintf(msg, "illegal roman numeral: '%c'. position: %d", c, pos);
  e->messages[e->count] = msg;
  e->count++;
}

// Print errors
void print_elist(ErrorList e) {
  int i;
  for (i = 0; i < e.count; i++) {
    printf("%s\n", e.messages[i]);
  }
}
