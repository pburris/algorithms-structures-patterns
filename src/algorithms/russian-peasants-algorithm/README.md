# Russian Peasants Algorithm

The Russian Peasants Algorithm, also known as [Egyptian Multiplication](https://en.wikipedia.org/wiki/Ancient_Egyptian_multiplication) is a multiplication algorithm that uses powers of 2 and the divide and conquer strategy to create an efficient multiplication algorithm.

## Example

More Computations than needed. Completely linear.
```
function(x, y) {
  // Given x and y we want to return x * y
  // A simple way of implementing this is just to add x to itself y times with a loop
  final_answer = 0
  for(i = 0; i < y; i++) {
    final_answer = final_answer + x
  }
  return final_answer
}

```

A lot less computations than above. If you were to graph the number of computations it would look like log base 2 + 1 for the last step
```

function(x, y) {
  // Given x and y we want to return x * y
  // The more efficient solution is to double the y value and half the x while checking if the x
  // value is even or odd. If the x value is odd we will add the y value to our running total
  final_answer = 0
  while (x > 0) {
    if (x % 2 !== 0) {
      final_answer = final_answer + y
    }
    x = x / 2
    y = y * 2
  }
  return final_answer
}

```
