
def russian_peasants_algorithm(a, b)
  x = a
  y = b
  z = 0
  while x > 0 do

    if x % 2 == 1
      z = z + y
    end

    y = y << 1
    x = x >> 1

  end
  return z
end
