require "./russian_peasants_algorithm.rb"
require "test/unit"

class RussianPeasantsAlgorithm < Test::Unit::TestCase

  def test_multiply
    assert_equal(russian_peasants_algorithm(4, 3), 12)
  end

  def test_compound
    a = russian_peasants_algorithm(7, 2)
    b = russian_peasants_algorithm(2, 5)
    compound = russian_peasants_algorithm(a, b)
    assert_equal(compound, 140)
  end

  def test_versus_native
    assert_equal(russian_peasants_algorithm(41268, 123687), 41268 * 123687)
  end

end
