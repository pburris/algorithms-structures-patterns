import unittest
import russian_peasants_algorithm as rpa


class AlgorithmTest(unittest.TestCase):
    def test_multiply(self):
        self.assertEqual(rpa.algorithm(3,4), 12)

    def test_compound(self):
        a = rpa.algorithm(7, 2)
        b = rpa.algorithm(2, 5)
        compound = rpa.algorithm(a, b)
        self.assertEqual(compound, 140)

    def test_versus_native(self):
        self.assertEqual(rpa.algorithm(76921, 55931), 76921 * 55931)
