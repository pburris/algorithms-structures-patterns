package main

func russian_peasants_algorithm(x, y int) (z int) {
  for x > 0 {
    if x % 2 != 0 {
      z = z + y
    }
    y <<= 1
    x >>= 1
  }
  return
}

func main() {
}
