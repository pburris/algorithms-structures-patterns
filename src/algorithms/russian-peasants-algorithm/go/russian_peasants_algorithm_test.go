package main

import (
  "testing"
  "github.com/stretchr/testify/assert"
)


func TestMultiply(t *testing.T) {
  assert.Equal(t, russian_peasants_algorithm(2, 3), 6, "2 * 3 should equal 6")
}

func TestCompound(t *testing.T) {
  first_val := russian_peasants_algorithm(7, 2)
  second_val := russian_peasants_algorithm(2, 5)
  assert.Equal(t, first_val * second_val, 140, "((7 * 2) * (5 * 2)) should equal 140")
}

func TestVersusNative(t *testing.T) {
  assert.Equal(t, russian_peasants_algorithm(68315, 206783), 68315 * 206783, "(68315 * 206783) should equal 14126380645")
}
