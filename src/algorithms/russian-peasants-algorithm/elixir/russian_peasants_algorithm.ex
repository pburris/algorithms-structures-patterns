use Bitwise, only_operators: true

defmodule RussianPeasantsModule do

  def algorithm(x, y, z \\ 0)

  def algorithm(x, y, z) when x > 0 do
    cond do
      rem(x, 2) != 0 ->
        algorithm(x >>> 1, y <<< 1, z + y)
      rem(x, 2) == 0 ->
        algorithm(x >>> 1, y <<< 1, z)
    end
  end

  def algorithm(x, y, z) when z > 0 and x == 0 do
    z
  end

end
