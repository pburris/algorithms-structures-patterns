ExUnit.start()

defmodule RussianPeasantsTest do
  use ExUnit.Case
  require RussianPeasantsModule

  test "Simple Multiplication" do
    assert RussianPeasantsModule.algorithm(2, 3) == 6
  end

  test "Compound" do
    value_one = RussianPeasantsModule.algorithm(7, 2)
    value_two = RussianPeasantsModule.algorithm(2, 5)
    assert (value_one * value_two) == 140
  end

  test "Simple Multiplication Versus Native Function" do
    assert RussianPeasantsModule.algorithm(29566, 185663) == (29566 * 185663)
  end

end
