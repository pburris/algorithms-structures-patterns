const assert = require('assert');
const test = require('ava').test;
const russianPeasantsAlgorithm = require('./russian_peasants_algorithm');


test("Basic Multiplication", function(t) {
  t.deepEqual(russianPeasantsAlgorithm(3, 4), 12);
});

test("Compound Multiplication", function(t) {
  var a = russianPeasantsAlgorithm(7, 2);
  var b = russianPeasantsAlgorithm(2, 5);
  var compound = russianPeasantsAlgorithm(a, b);
  t.deepEqual(russianPeasantsAlgorithm(a, b), 140);
  console.log("Compound test passed");
});

test("Multiplication Versus Native", function(t) {
  var bigA = 12034.0;
  var bigB = 60732.0;
  t.deepEqual(russianPeasantsAlgorithm(bigA, bigB), bigA * bigB);
});
