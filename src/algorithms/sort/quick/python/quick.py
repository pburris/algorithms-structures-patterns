

def quick_sort(_arr):
    arr = _arr[:]
    return quick_sort_help(arr, 0, len(arr) - 1)

def quick_sort_help(_arr, first, last):
    arr = _arr[:]

    if first < last:
        splitpoint = partition(arr, first, last)
        arr = quick_sort_help(arr, first, splitpoint - 1)
        arr = quick_sort_help(arr, splitpoint + 1, last)

    return arr

def partition(arr, first, last):
    pivot = arr[first]
    left = first + 1
    right = last
    done = False

    while not done:
        while left <= right and arr[left] <= pivot:
            left += 1

        while arr[right] >= pivot and right >= left:
            right -= 1

        if right < left:
            done = True
        else:
            temp = arr[left]
            arr[left] = arr[right]
            arr[right] = temp
    temp = arr[first]
    arr[first] = arr[right]
    arr[right] = temp
    return right



u_arr = [1,5,2,4,7,9,10,6,8,3]
s_arr = [1,2,3,4,5,6,7,8,9,10]

print(s_arr == quick_sort(u_arr))
