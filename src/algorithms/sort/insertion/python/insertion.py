

def insertion_sort(_arr):
    arr = _arr[:]
    for position in range(1, len(arr)):
        currentvalue = arr[position]

        while position > 0 and arr[position - 1]> currentvalue:
            arr[position] = arr[position - 1]
            position = position - 1

        arr[position] = currentvalue
    return arr

u_arr = [1,5,2,4,7,9,10,6,8,3]
s_arr = [1,2,3,4,5,6,7,8,9,10]

print(s_arr == insertion_sort(u_arr))
