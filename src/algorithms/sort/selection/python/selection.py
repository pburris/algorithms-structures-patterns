

def selection_sort(_arr):
    arr = _arr
    for fillslot in range(len(arr) - 1, 0, -1):
        positionOfMax = 0
        for location in range(1, fillslot + 1):
            if arr[location] > arr[positionOfMax]:
                positionOfMax = location

        temp = arr[fillslot]
        arr[fillslot] = arr[positionOfMax]
        arr[positionOfMax] = temp
    return arr


u_arr = [1,5,2,4,7,9,10,6,8,3]
s_arr = [1,2,3,4,5,6,7,8,9,10]

print(s_arr == selection_sort(u_arr))
