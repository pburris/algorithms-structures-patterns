

def bubble_sort(_arr):
    arr = _arr[:]
    for length in range(len(arr) - 1, 0, -1):
        for i in range(length):
            if arr[i] > arr[i + 1]:
                temp = arr[i]
                arr[i] = arr[i + 1]
                arr[i + 1] = temp
    return arr



u_arr = [1,5,2,4,7,9,10,6,8,3]
s_arr = [1,2,3,4,5,6,7,8,9,10]

print(s_arr == bubble_sort(u_arr))
