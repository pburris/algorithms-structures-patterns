

def shell_sort(_arr):
    arr = _arr[:]
    sublistcount = int(len(arr) / 2)
    while sublistcount > 0:
        for start in range(sublistcount):
            arr = gap_insertion_sort(arr, start, sublistcount)
        sublistcount = int(sublistcount / 2)
    return arr

def gap_insertion_sort(_arr, start, gap):
    arr = _arr[:]
    for i in range(start + gap, len(arr), gap):
        currentvalue = arr[i]
        position = i
        while position >= gap and arr[position - gap] > currentvalue:
            arr[position] = arr[position - gap]
            position = position - gap

        arr[position] = currentvalue
    return arr



u_arr = [1,5,2,4,7,9,10,6,8,3]
s_arr = [1,2,3,4,5,6,7,8,9,10]

print(s_arr == shell_sort(u_arr))
