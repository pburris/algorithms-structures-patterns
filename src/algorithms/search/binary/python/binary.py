

def binary_search(arr, element):
    if len(arr) == 0:
        return False
    else:
        mid = int(len(arr) / 2)
        if arr[mid] == element:
            return True
        else:
            if element < arr[mid]:
                return binary_search(arr[:mid], element)
            else:
                return binary_search(arr[mid + 1:], element)

arr = [1,2,3,4,5,6,7,8,9,10,11,12,13]

print(binary_search(arr, 6))
print(binary_search(arr, 100))
