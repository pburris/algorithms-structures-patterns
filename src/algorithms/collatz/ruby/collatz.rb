
def nextN(n)
    if (n % 2) == 0 then
      return n / 2
    end
    return (n * 3) + 1
end

def findCollatz(n, count = 1)
  if (n == 1) then
    return count
  end
  return findCollatz(nextN(n), count + 1)
end

argv = ARGV

if (argv.length != 1) then
  puts findCollatz(10)
else
  puts findCollatz(ARGV[0].to_i)
end
