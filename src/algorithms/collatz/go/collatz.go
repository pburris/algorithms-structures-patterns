package main

import (
	"fmt"
	"os"
	"strconv"
)

func nextN(n int) int {
	if n%2 == 0 {
		return n / 2
	}
	return (n * 3) + 1
}

func col(n int, count int) int {
	if n == 1 {
		return count
	}
	return col(nextN(n), count+1)
}

func collatz(n int) int {
	count := 1
	return col(n, count)
}

func main() {
	args := os.Args
	if len(args) < 2 {
		fmt.Printf("%d\n", collatz(10))
	} else {
		i, err := strconv.Atoi(args[1])
		if err != nil {
			panic(err)
		}

		fmt.Printf("%d\n", collatz(i))
	}
}
