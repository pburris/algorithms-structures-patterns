/**
 * Collatz Conjecture
 */

const nextN = n => (n % 2 === 0 ? n / 2 : n * 3 + 1);

const findCollatz = (n, count = 1) => {
  if (n === 1) return count;
  return findCollatz(nextN(n), count + 1);
};

const args = process.argv;

if (args.length < 3) {
  console.log(findCollatz(10));
} else {
  console.log(findCollatz(Number(args[2])));
}
