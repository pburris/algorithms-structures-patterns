import sys

def nextN(n):
    if (n % 2) == 0:
        return n /2
    return (n * 3) + 1

def findCollatz(n, count = 1):
    if (n == 1): return count
    return findCollatz(nextN(n), count + 1)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(findCollatz(10))
    else:
        print(findCollatz(int(sys.argv[1])))
