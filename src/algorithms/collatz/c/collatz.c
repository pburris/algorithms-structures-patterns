/**
 * Collatz Conjecture
 */
#include <stdio.h>
#include <stdlib.h>

int nextN(int n) {
  if (n % 2 == 0) {
    return n / 2;
  }
  return (n * 3) + 1;
};

int findCollatz(int n, int count) {
  if (n == 1) {
    return count;
  }
  return findCollatz(nextN(n), count + 1);
}

int main(int argc, char** argv) {
  if (argc < 2) {
    printf("%d\n", findCollatz(10, 1));
    return 0;
  } else {
    printf("%d\n", findCollatz(atoi(argv[1]), 1));
  }
}
