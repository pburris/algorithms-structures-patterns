# Arrays

Arrays are simple data structures that act as containers for other types of data types. An array is comprised of elements, or each item in the array. Each element has an index starting with 0 and increasing until the end of the array is reached. You use the index to quickly look up and access a given element.

## TODO:

I need to clean up the python files so they aren't trying to execute any code and instead are just imported into a test file


#### Basic Operations

  * **Traverse** - return each element, one by one.
  * **Insertion** - Add element at a given index
  * **Deletion** - Delete an element at a given index
  * **Search** - Searches for an element given the index or value
  * **Update** - Update an element given the index
