require "./array.rb"
require "test/unit"

class Array_Test < Test::Unit::TestCase

  def test_deletion
    arr = Array_.new([1,2,3,4,5,6])
    assert_equal(arr.deletion(2), [1,2,4,5,6])
  end

  def test_pop
    arr = Array_.new([1,2,3,4,5,6])
    arr.pop()
    assert_equal(arr.items, [1,2,3,4,5])
  end

  def test_shift
    arr = Array_.new([1,2,3,4,5,6])
    arr.shift()
    assert_equal(arr.items, [2,3,4,5,6])
  end

  def test_insert
    arr = Array_.new([1,2,3,4,5,6])
    assert_equal(arr.insert("cat", 2), [1,2,"cat",3,4,5,6])
  end

  def test_push
    arr = Array_.new([1,2,3,4,5,6])
    assert_equal(arr.push("dog"), [1,2,3,4,5,6,"dog"])
  end

  def test_unshift
    arr = Array_.new([1,2,3,4,5,6])
    assert_equal(arr.unshift("house"), ["house",1,2,3,4,5,6])
  end

  def test_search_true
    arr = Array_.new([1,2,3,4,5,6])
    assert_equal(arr.search(5), 4)
  end

  def test_search_false
    arr = Array_.new([1,2,3,4,5,6])
    assert_equal(arr.search("house"), -1)
  end

  def test_update
    arr = Array_.new([1,2,3,4,5,6])
    assert_equal(arr.update("house", 3), [1,2,3,"house",5,6])
  end

end
