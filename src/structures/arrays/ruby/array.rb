
class Array_
  attr_accessor :items

  def initialize(items)
    @items = items
  end

  def new(items)
    @items = items
  end

  def deletion(slot)
    length = @items.length
    new_arr = @items
    while slot < length do
      new_arr[slot] = new_arr[slot + 1]
      slot += 1
    end
    new_arr = new_arr.slice(0, @items.length - 1)
    @items = new_arr
    return @items
  end

  def shift
    first = @items[0]
    self.deletion(0)
    return first
  end

  def pop
    last = @items[@items.length - 1]
    self.deletion(@items.length - 1)
    return last
  end

  def insert(item, slot)
    length = @items.length
    new_arr = @items
    while length >= slot do
      new_arr[length + 1] = new_arr[length]
      length -= 1
    end
    new_arr[slot] = item
    @items = new_arr.slice(0, @items.length - 1)
    return @items
  end

  def push(item)
    return self.insert(item, @items.length)
  end

  def unshift(item)
    return self.insert(item, 0)
  end

  def search(item)
    ret = false
    new_arr = @items
    for i in 0..(@items.length - 1) do
      if (new_arr[i] == item)
        ret = i
        break
      end
    end
    return ret ? ret : -1
  end

  def update(item, slot)
    length = @items.length
    new_arr = @items
    for i in 0..(@items.length - 1) do
      if (i == slot)
        new_arr[i] = item
        break
      end
    end
    @items = new_arr
    return @items
  end

end
