var array = {};

/**
 * Array: Update
 *
 * @description Basic linear search to find and then update item in the array
 * @param {Array} array, array to insert item into
 * @param {Object} item, value to search for
 * @return {Array} arrCopy, a new array with the inserted value
 */
array.update = function(arr, item, slot) {
  var newArray = arr;
  newArray[slot] = item;
  return newArray;
}

module.exports = array;
