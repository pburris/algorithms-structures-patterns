
const array = Object.assign({},
  require('./deletion'),
  require('./insertion'),
  require('./search'),
  require('./update')
);

module.exports = array;
