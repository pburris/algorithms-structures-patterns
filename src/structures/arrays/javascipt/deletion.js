var array = {};

/**
 * Array: Deletion
 *
 * @description Remove an item at a given slot and then rearange the remaining items
 * @param {Array} array, array to insert item into
 * @param {Object} item, any item that can be inserted into an array
 * @return {Array} arrCopy, a new array with the inserted value
 */
array.deletion = function(array, slot) {
  var length = array.length;
  var arrCopy = array;

  while (slot < length) {
    arrCopy[slot - 1] = arrCopy[slot];
    slot += 1;
  }
  // return array minus last item
  return arrCopy.slice(0, length - 1);
}

/**
 * Array: Shift
 *
 * @description Mimics Array.shift() that removes an item from the front of the array
 * @param {Array} array, array to remove item from
 * @return {Array} arrCopy, a new array with the inserted value
 */
array.shift = function(array) {
  return this.deletion(array, 1);
}

/**
 * Array: Pop
 *
 * @description Mimics Array.pop() that removes an item from the end of the array
 * @param {Array} array, array to remove item from
 * @return {Array} arrCopy, a new array with the inserted value
 */
array.pop = function(array) {
  return this.deletion(array, array.length);
}

module.exports = array;
