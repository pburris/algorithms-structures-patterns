var array = {};

/**
 * Array: Insert
 *
 * @description Insert item into a given array at a given slot number
 * @param {Array} array, array to insert item into
 * @param {Object} item, any item that can be inserted into an array
 * @param {Number} slot, interger index for the array
 * @return {Array} arrCopy, a new array with the inserted value
 */
array.insert = function(array, item, slot) {
  var length = array.length;
  var arrCopy = array;

  while (length >= slot) {
    arrCopy[length] = arrCopy[length - 1];
    length -= 1;
  }
  arrCopy[slot] = item;
  return arrCopy;
}

/**
 * Array: Push
 *
 * @description Mimics Array.push() which adds an item onto the end of the array
 * @param {Array} array, array to insert item into
 * @param {Object} item, any item that can be inserted into an array
 * @return {Array} arrCopy, a new array with the inserted value
 */
array.push = function(array, item) {
  return this.insert(array, item, array.length);
}

/**
 * Array: Unshift
 *
 * @description Mimics Array.unshift() which adds an item onto the beginning of the array
 * @param {Array} array, array to insert item into
 * @param {Object} item, any item that can be inserted into an array
 * @return {Array} arrCopy, a new array with the inserted value
 */
array.unshift = function(array, item) {
  return this.insert(array, item, 0);
}

module.exports = array;
