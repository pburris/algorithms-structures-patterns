const test = require('ava').test;
const array = require('./array');


/**
 * array::deletion
 * array::pop
 * array::shift
 * array::insert
 * array::push
 * array::unshift
 * array::search
 * array::update
 */

test("array::deletion", function(t) {
  var arr = [1,2,3,4,5,6];
  var newArr = array.deletion(arr, 2);
  t.deepEqual(newArr, [1,3,4,5,6]);
});

test("array::pop", function(t) {
  var arr = [1,2,3,4,5,6];
  var newArr = array.pop(arr);
  t.deepEqual(newArr, [1,2,3,4,5]);
});

test("array::shift", function(t) {
  var arr = [1,2,3,4,5,6];
  var newArr = array.shift(arr);
  t.deepEqual(newArr, [2,3,4,5,6]);
});

test("array::insert", function(t) {
  var arr = [1,2,3,4,5,6];
  var newArr = array.insert(arr, "cat", 3);
  t.deepEqual(newArr, [1,2,3, "cat", 4,5,6]);
});

test("array::push", function(t) {
  var arr = [1,2,3,4,5,6];
  var newArr = array.push(arr, "cat");
  t.deepEqual(newArr, [1,2,3,4,5,6, "cat"]);
});

test("array::unshift", function(t) {
  var arr = [1,2,3,4,5,6];
  var newArr = array.unshift(arr, "cat");
  t.deepEqual(newArr, ["cat", 1,2,3,4,5,6]);
});

test("array::search, false", function(t) {
  var arr = [1,2,3,4,5,6];
  var index = array.search(arr, "cat");
  t.deepEqual(index, -1);
});

test("array::search, true", function(t) {
  var arr = [1,2,3, "cat", 4,5,6];
  var index = array.search(arr, "cat");
  t.deepEqual(index, 3);
});

test("array::update", function(t) {
  var arr = [1,2,3,4,5,6];
  var newArr = array.update(arr, "cat", 3);
  t.deepEqual(newArr, [1,2,3, "cat", 5,6]);
});
