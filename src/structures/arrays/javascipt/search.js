var array = {};

/**
 * Array: Search
 *
 * @description Basic linear search to find an index based on a given value
 * @param {Array} array, array to insert item into
 * @param {Object} item, value to search for
 * @return {Number} arrCopy, index if the item is in the array, otherwise -1
 */
array.search = function(array, item) {
  var ret = null;
  for (var i = 0; i < array.length; i++) {
    if (array[i] === item) {
      ret = i;
      break;
    }
  }
  return ret ? ret : -1;
}

module.exports = array;
