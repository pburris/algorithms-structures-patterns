

"""Array

Data structure that linearly holds data
"""
class Array:

    def __init__(self, items):
        self.items = items

    """Deletion

    Remove an item at a given slot and then rearange the remaining items
    """
    def deletion(self, slot):
        length = len(self.items)
        new_arr = self.items
        while slot < length:
            new_arr[slot - 1] = new_arr[slot]
            slot += 1
        new_arr = new_arr[0:len(new_arr) - 1]
        self.items = new_arr
        return self.items


    """Shift

    Remove the item from the front of the array
    """
    def shift(self):
        first = self.items[0]
        self.deletion(1)
        return first;

    """Pop

    Remove the item from the end of the array
    """
    def pop(self):
        last = self.items[len(self.items) - 1]
        self.deletion(len(self.items))
        return last

    """Insert

    Insert item into a given array at a given slot number
    """
    def insert(self, item, slot):
        print(self.items)
        length = len(self.items) - 1
        new_arr = self.items + [0]
        while length >= slot:
            new_arr[length + 1] = new_arr[length]
            length -= 1
        new_arr[slot] = item
        self.items = new_arr
        return self.items

    """Push

    Add an item onto the end of the array
    """
    def push(self, item):
        return self.insert(item, len(self.items))

    """Unshift

    Add an item onto the beginning of the array
    """
    def unshift(self, item):
        return self.insert(item, 0)

    """Search

    Basic linear search to find an index based on a given value
    """
    def search(self, item):
        ret = False
        new_arr = self.items
        i = 0
        for i in range(0, len(new_arr) - 1):
            if new_arr[i] == item:
                ret = i
                break
        return ret if ret else -1

    """Update

    Basic linear search to find and then update item in the array
    """
    def update(self, item, slot):
        length = len(self.items)
        new_arr = self.items
        for i in range(0, length - 1):
            if i == slot:
                new_arr[i] = item
                break
        return new_arr
