

def finder(arr1, arr2):
    bigger = arr1 if len(arr1) > len(arr2) else arr2
    smaller = arr2 if len(arr2) < len(arr1) else arr1
    ret = list()
    for item in bigger:
        if not item in smaller:
            ret.append(item)
    return ret


print(finder([1,2,3,4,5,6,7,10,13],[3,7,2,1,4,6,5,44]))
