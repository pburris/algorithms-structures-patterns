
def sanatize(string):
    return "".join(letter for letter in list(string) if letter.isalpha()).lower()


def alpha_sort(string):
    characters = list(sanatize(string))
    for j in range(1, len(characters)):
        for i in range(1, len(characters)):
            if ord(characters[i]) < ord(characters[i - 1]):
                temp_character = characters[i - 1]
                characters[i - 1] = characters[i]
                characters[i] = temp_character
    return characters;



def anagram(str1, str2):
    if not len(sanatize(str1)) == len(sanatize(str2)):
        return False
    return alpha_sort(str1) == alpha_sort(str2)


print(anagram('go go go','gggooo'))
print(anagram('hi man','hi     man'))
print(anagram("public relations", "crap built on lies."))
