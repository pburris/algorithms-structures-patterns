
def unique_string(s):
    string_array = list(s)
    letters = list()
    for char in string_array:
        if not char in letters:
            letters.append(char)
        else:
            return False
    return True

print(unique_string('go'))
