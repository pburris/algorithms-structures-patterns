

def compress(s):
    ret = ''
    length = len(s)
    count = 1
    index = 1

    if length == 0:
        return ret
    if length == 1:
        return s+'1'

    while index < length:
        if s[index] == s[index - 1]:
            count += 1
        else:
            ret = ret + s[index - 1] + str(count)
            count = 1
        index += 1
    return ret + s[index - 1] + str(count)



print(compress('AAAAABBBBCCCC'))
