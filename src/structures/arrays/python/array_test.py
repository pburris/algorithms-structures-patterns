import unittest
from array import Array


class AlgorithmTest(unittest.TestCase):
    def test_deletion(self):
        arr = Array([1, 2, 3, 4, 5, 6])
        self.assertEqual(arr.deletion(2), [1, 3, 4, 5, 6])

    def test_pop(self):
        arr = Array([1, 2, 3, 4, 5, 6])
        arr.pop()
        self.assertEqual(arr.items, [1, 2, 3, 4, 5])

    def test_shift(self):
        arr = Array([1, 2, 3, 4, 5, 6])
        arr.shift()
        self.assertEqual(arr.items, [2, 3, 4, 5, 6])

    def test_insert(self):
        arr = Array([1, 2, 3, 4, 5, 6])
        self.assertEqual(arr.insert("cat", 4), [1, 2, 3, 4, "cat", 5, 6])

    def test_push(self):
        arr = Array([1, 2, 3, 4, 5, 6])
        self.assertEqual(arr.push("cat"), [1, 2, 3, 4, 5, 6, "cat"])

    def test_unshift(self):
        arr = Array([1, 2, 3, 4, 5, 6])
        self.assertEqual(arr.unshift("cat"), ["cat", 1, 2, 3, 4, 5, 6])

    def test_search_true(self):
        arr = Array([1, 2, 3, 4, 5, 6])
        self.assertEqual(arr.search(5), 4)

    def test_search_false(self):
        arr = Array([1, 2, 3, 4, 5, 6])
        self.assertEqual(arr.search("dog"), -1)

    def test_update(self):
        arr = Array([1, 2, 3, 4, 5, 6])
        self.assertEqual(arr.update("house", 2), [1, 2, "house", 4, 5, 6])
