
def de_dupe(arr):
    ret = list()
    for item in arr:
        item_reversed = [item[1], item[0]]
        if not item in ret and not item_reversed in ret:
            ret.append(item)
    return ret


def pair_sum(arr, total):
    if len(arr) < 2:
        return
    ret = list()
    for i in range(0, len(arr)):
        for j in range(0, len(arr)):
            if not i == j:
                if arr[i] + arr[j] == total:
                    ret.append([arr[i], arr[j]])
    return de_dupe(ret)

print(pair_sum([1,9,2,8,3,7,4,6,5,5,13,14,11,13,-1],10))
