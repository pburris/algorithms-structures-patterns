

def reverse(sentence):
    reversed_sentence = list()
    sentence_array = sentence.split(' ')
    filtered_array = list(filter(lambda word: not word == '', sentence_array))
    for i in range(0, len(filtered_array)):
        reversed_sentence.append(filtered_array.pop())
    return reversed_sentence


print(reverse('  This is the   best   '))
