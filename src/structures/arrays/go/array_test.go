package main

import (
  "testing"
  "github.com/stretchr/testify/assert"
)

func TestDeletion(t *testing.T) {
  var arr []int = []int{1,2,3,4,5,6}
  assert.Equal(t, deletion(arr, 2), []int{1,2,4,5,6}, "Deletion should delete the item in index 3")
}

func TestPop(t *testing.T) {
  var arr []int = []int{1,2,3,4,5,6}
  assert.Equal(t, pop(arr), []int{1,2,3,4,5}, "Pop the last item off of the array")
}

func TestShift(t *testing.T) {
  var arr []int = []int{1,2,3,4,5,6}
  assert.Equal(t, shift(arr), []int{2,3,4,5,6}, "Shift first item off")
}

func TestInsert(t *testing.T) {
  var arr []int = []int{1,2,3,4,5,6}
  assert.Equal(t, insert(arr, 14, 3), []int{1,2,3,14,4,5,6}, "Insert number 14 in index 3")
}

func TestPush(t *testing.T) {
  var arr []int = []int{1,2,3,4,5,6}
  assert.Equal(t, push(arr, 144), []int{1,2,3,4,5,6,144}, "Insert item to the end of the arry")
}

func TestUnshift(t *testing.T) {
  var arr []int = []int{1,2,3,4,5,6}
  assert.Equal(t, unshift(arr, 1234), []int{1234,1,2,3,4,5,6}, "Insert item in the beginning of the array")
}

func TestSearchTrue(t *testing.T) {
  var arr []int = []int{1,2,3,4,5,6}
  assert.Equal(t, search(arr, 4), 3, "Search for number 4 in array and return index 3 when found")
}

func TestSearchFalse(t *testing.T) {
  var arr []int = []int{1,2,3,4,5,6}
  assert.Equal(t, search(arr, 144), -1, "Search for number 144 and return -1 when not found")
}

func TestUpdate(t *testing.T) {
  var arr []int = []int{1,2,3,4,5,6}
  assert.Equal(t, update(arr, 144, 2), []int{1,2,144,4,5,6}, "Update index 2 to 144")
}
