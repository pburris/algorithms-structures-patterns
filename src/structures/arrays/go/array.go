package main


func deletion(arr []int, slot int) []int {
  length := len(arr)
  slot = slot + 1
  for slot < length {
    arr[slot - 1] = arr[slot]
    slot += 1
  }
  return arr[:length - 1]
}

func pop(arr []int) []int {
  return deletion(arr, len(arr))
}

func shift(arr []int) []int {
  return deletion(arr, 0)
}

func insert(arr []int, item int, slot int) []int {
  length := len(arr)
  var new_arr []int = append(append(arr, 0), 0)
  for length >= slot {
    new_arr[length + 1] = new_arr[length]
    length -= 1
  }
  new_arr[slot] = item
  return new_arr[:len(new_arr) - 1]
}

func push(arr []int, item int) []int {
  return insert(arr, item, len(arr))
}

func unshift(arr []int, item int) []int {
  return insert(arr, item, 0)
}

func search(arr []int, item int) int {
  ret := -1
  for i := 0; i < len(arr); i++ {
    if i == item {
      ret = i - 1
      break
    }
  }
  return ret
}

func update(arr []int, item int, slot int) []int {
  length := len(arr)
  for i := 0; i < length; i++ {
    if i == slot {
      arr[i] = item
      break
    }
  }
  return arr
}

func main() {
}
