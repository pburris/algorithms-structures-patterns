ExUnit.start()

defmodule ArrayTest do
  use ExUnit.Case
  import Array

  test "Deletion" do
    arr = [1,2,3,4,5,6]
    assert Array.deletion(arr, 3) == [1,2,3,5,6]
  end

  test "Pop" do
    arr = [1,2,3,4,5,6]
    assert Array.pop(arr) == [1,2,3,4,5]
  end

  test "Shift" do
    arr = [1,2,3,4,5,6]
    assert Array.shift(arr) == [2,3,4,5,6]
  end

  test "Insert" do
    arr = [1,2,3,4,5,6]
    assert Array.insert(arr, 10, 3) == [1,2,3,4,10,5,6]
  end

  test "Push" do
    arr = [1,2,3,4,5,6]
    assert Array.push(arr, 144) == [1,2,3,4,5,6,144]
  end

  test "Unshift" do
    arr = [1,2,3,4,5,6]
    assert Array.unshift(arr, 144) == [144,1,2,3,4,5,6]
  end

  test "Search, True" do
    arr = [1,2,3,4,5,6]
    assert Array.search(arr, 3) == 2
  end

  test "Search, False" do
    arr = [1,2,3,4,5,6]
    assert Array.search(arr, 144) == -1
  end

  test "Update" do
    arr = [1,2,3,4,5,6]
    assert Array.update(arr, 150, 2) == [1,2,150,4,5,6]
  end

end
