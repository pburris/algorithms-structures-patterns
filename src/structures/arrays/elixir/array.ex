
defmodule Array do

  def deletion(arr, slot) do
    len = length arr
    cond do
      slot >= len ->
        List.flatten(arr)
      slot < len ->
        deletion([ Enum.slice(arr, 0..slot - 1) | Enum.slice(arr, slot + 1..length(arr)) ], slot + 1)
    end
  end

  def pop(arr) do
    deletion(arr, length(arr) - 1)
  end

  def shift(arr) do
    deletion(arr, 0)
  end

  def insert(arr, item, slot) do
    arr
  end

  def push(arr, item) do
    insert(arr, item, length(arr) - 1)
  end

  def unshift(arr, item) do
    insert(arr, item, 0)
  end

  def search(arr, item) do
    arr
  end

  def update(arr, item, slot) do
    arr
  end

end
