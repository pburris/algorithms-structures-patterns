const OPS = {
  PUSH: 'PUSH',
  ADD: 'ADD',
  MINUS: 'MINUS',
};

class VirtualMachine {
  constructor(program) {
    this.pc = 0;
    this.stack = [];
    this.sp = 0;
    this.program = program;
  }

  run() {
    while (this.pc < this.program.length) {
      const current = this.program[this.pc];
      switch (current) {
        case OPS.PUSH: {
          this.stack[this.sp] = this.program[this.pc + 1];
          this.sp += 1;
          this.pc += 1;
          break;
        }

        case OPS.ADD: {
          const right = this.stack[this.sp - 1];
          this.sp -= 1;
          const left = this.stack[this.sp - 1];
          this.sp -= 1;
          this.stack[this.sp] = left + right;
          this.sp += 1;
          break;
        }

        case OPS.MINUS: {
          const right = this.stack[this.sp - 1];
          this.sp -= 1;
          const left = this.stack[this.sp - 1];
          this.sp -= 1;
          this.stack[this.sp] = left - right;
          this.sp += 1;
          break;
        }

        default:
          this.sp += 1;
      }
      this.pc += 1;
    }

    console.log(`Stack top: ${this.stack[this.sp - 1]}`);
  }
}

const test = () => {
  const { PUSH, ADD, MINUS } = OPS;
  const program = [PUSH, 3, PUSH, 4, ADD, PUSH, 5, MINUS];
  const vm = new VirtualMachine(program);

  vm.run();
};

test();
