
class Deque(object):

    def __init__(self):
        self.items = []

    def size(self):
        return len(self.items)

    def isEmpty(self):
        return self.items == []

    def addFront(self, item):
        self.items.append(item)
        return self.items

    def removeFront(self):
        return self.items.pop()

    def addRear(self, item):
        self.items.insert(0, item)
        return self.items

    def removeRear(self):
        return self.items.pop(0)
