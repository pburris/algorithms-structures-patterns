"""
    Binary Heap
"""
class BinaryHeap(object):

    def __init__(self):
        self.heaplist = [0]
        self.currentSize = 0

    """
        BinaryHeap::percUp

        Percolate up the tree to rebalance given a tree size
    """
    def percUp(self, i):
        while i // 2 > 0:
            if self.heaplist[i] < self.heaplist[i // 2]:
                tmp = self.heaplist[i // 2]
                self.heaplist[i // 2] = self.heaplist[i]
                self.heaplist[i] = tmp
            i = i // 2

    """
        BinaryHeap::minChild

        Get minimum child effected while percolating down the tree
    """
    def minChild(self, i):
        if i * 2 + 1 > self.currentSize:
            return i * 2
        else:
            if self.heaplist[i * 2] < self.heaplist[i * 2 + 1]:
                return i * 2
            else:
                return i * 2 + 1

    """
        BinaryHeap::percDown

        Percolate down the tree to rebalance after a removal given tree size
    """
    def percDown(self, i):
        while (i * 2) <= self.currentSize:
            mc = self.minChild(i)
            if self.heaplist[i] > self.heaplist[mc]:
                tmp = self.heaplist[i]
                self.heaplist[i] = self.heaplist[mc]
                self.heaplist[mc] = tmp
            i = mc

    """
        BinaryHeap::insert

        Insert node into binary heap
    """
    def insert(self, item):
        self.heaplist.append(item)
        self.currentSize += 1
        self.percUp(self.currentSize)

    """
        BinaryHeap::delMin

        Del Min
    """
    def delMin(self):
        ret = self.heaplist[1]
        self.heaplist[1] = self.heaplist[self.currentSize]
        self.currentSize = self.currentSize - 1
        self.heaplist.pop()
        self.percDown(1)
        return ret

    """
        BinaryHeap::isEmpty

        Checks if the heap is empty
    """
    def isEmpty(self):
        return self.heaplist == [0]

    """
        BinaryHeap::size

        Return the size of the heap
    """
    def size(self):
        return self.currentSize

    """
        BinaryHeap::buildHeap

        Buld Heap
    """
    def buildHeap(self, keys):
        i = len(keys) // 2
        self.currentSize = len(keys)
        self.heaplist = [0] + keys[:]
        while (i > 0):
            self.percDown(i)
            i = i - 1
