type EqualityFn<T> = (a: T, b: T) => boolean;

interface INode {
  toString(): string;
}

export default class Tree<T extends INode> {
  public left: Tree<T> | null;
  public right: Tree<T> | null;

  private _depth: number = 0;

  constructor(
    public value: T,
    public equality: EqualityFn<T>,
    private parent?: Tree<T> | null
  ) {
    this.left = null;
    this.right = null;
  }

  public get depth(): number {
    return this._depth;
  }

  public insert(val: T): void {
    const { equality } = this;
    this.balance();
    if (this.left && this.right) {
      this._depth += 1;
      if (this.left.depth === this.right.depth) {
        this.left.insert(val);
      } else if (this.left.depth > this.right.depth) {
        this.right.insert(val);
      } else if (this.left.depth < this.right.depth) {
        this.left.insert(val);
      }
    }
    if (!this.left && !this.right) {
      this.left = new Tree(val, equality, this);
      return;
    }
    if (!this.left && this.right) {
      this.left = this.right;
      this.right = new Tree(val, equality, this);
      return;
    }
    if (this.left && !this.right) {
      this.right = new Tree(val, equality, this);
      return;
    }
  }

  public search(val: T): Tree<T> | null {
    const { equality } = this;
    if (equality(val, this.value)) return this;
    if (this.left && !this.right) {
      return this.left.search(val);
    }
    if (!this.left && this.right) {
      return this.right.search(val);
    }
    if (this.left && this.right) {
      return this.right.search(val) || this.left.search(val);
    }

    return null;
  }

  public balance() {
    if (this.left && this.left.value === null) delete this.left;
    if (this.right && this.right.value === null) delete this.right;
    if (this.left) this.left.balance();
    if (this.right) this.right.balance();
  }

  public swapValues(a: T, b: T): void {
    const tA = this.search(a);
    const tB = this.search(b);
    if (tA && tB) {
      const tmpVal = tA.value;
      tA.value = tB.value;
      tB.value = tmpVal;
    }
  }

  public remove(val: T): Tree<T> | null {
    const { equality } = this;
    if (val === this.value) {
      const cpy: Tree<T> = Object.assign(
        Object.create(Object.getPrototypeOf(this)),
        this
      );
      if (this.parent) {
        if (this.parent.left && equality(this.parent.left.value, val))
          this.parent.left = null;
        if (this.parent.right && equality(this.parent.right.value, val))
          this.parent.right = null;
      } else {
        this.left = null;
        this.right = null;
        this.value = {} as T;
      }
      return cpy;
    }
    if (this.left && !this.right) {
      return this.left.remove(val);
    }
    if (!this.left && this.right) {
      return this.right.remove(val);
    }
    if (this.left && this.right) {
      return this.right.remove(val) || this.left.remove(val);
    }
    return null;
  }

  public toString(pad: string = '|'): string {
    let str = '';
    str += `${pad} Root: ${this.value}\n`;

    if (this.left) {
      str += `${pad}- Left:\n`;
      str += this.left.toString(pad + '--');
    }

    if (this.right) {
      str += `${pad}- Right:\n`;
      str += this.right.toString(pad + '--');
    }

    return str;
  }
}
