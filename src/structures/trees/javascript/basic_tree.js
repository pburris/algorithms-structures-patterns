/**
 * Tree
 *
 * basic tree implementation
 */
module.exports = function Tree(root) {
  this.key = root;
  this.left = null;
  this.right = null;

  this.insertLeft = function(node) {
    if (this.left === null) {
      this.left = new Tree(node);
    } else {
      const t = new Tree(node);
      t.left = this.left;
      this.left = t;
    }
  };

  this.insertRight = function(node) {
    if (this.right === null) {
      this.right = new Tree(node);
    } else {
      const t = new Tree(node);
      t.right = this.right;
      this.left = t;
    }
  };

  this.getLeftChild = () => this.left;

  this.getRightChild = () => this.right;

  this.getRootVal = () => this.key;

  this.setRootVal = root => {
    this.key = root;
  };
};
