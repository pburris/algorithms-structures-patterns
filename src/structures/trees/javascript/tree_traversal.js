/**
 * Tree Traversal
 */
const Tree = require('./basic_tree');

const preorder = t => {
  if (t) {
    console.log(t.getRootVal());
    preorder(t.getLeftChild());
    preorder(t.getRightChild());
  }
};

const postorder = t => {
  if (t) {
    postorder(t.getLeftChild());
    postorder(t.getRightChild());
    console.log(t.getRootVal());
  }
};

const inorder = t => {
  if (t) {
    postorder(t.getLeftChild());
    console.log(t.getLeftChild());
    postorder(t.getRightChild());
  }
};

const t1 = new Tree(0);

t1.insertLeft(1);
t1.insertRight(4);

const t2 = t1.getLeftChild();
t2.insertLeft(2);
t2.insertRight(3);

const t3 = t1.getRightChild();
t3.insertLeft(5);
t3.insertRight(6);

preorder(t1);
postorder(t1);
inorder(t1);
