/**
 * Graph
 */
const STATE = {
  unvisited: 1,
  visited: 2,
  visiting: 3,
};

class Node {
  constructor(value) {
    this.value = value;
    this.visitState = STATE.unvisited;
    this.adjacent = {}; // node: weight
  }
}

class Graph {
  constructor() {
    this.nodes = {};
  }

  addNode(value) {
    const n = new Node(value);
    this.nodes[value] = n;
    return n;
  }

  addEdge(src, dest, weight = 0) {
    if (!Object.keys(this.nodes).includes(src)) {
      this.addNode(src);
    }
    if (!Object.keys(this.nodes).includes(dest)) {
      this.addNode(dest);
    }
    this.nodes[src].adjacent[this.nodes[dest]] = weight;
  }
}

module.exports = Graph;
