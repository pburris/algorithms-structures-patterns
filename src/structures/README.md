# Data Structures

### Table of Contents

  * Arrays
  * Graphs
  * Lists
  * Recursion
  * Stacks and Queues
  * Trees

### What is a data structure?

Data should be discrete, traceable, and easy to understand. Data has types like integer, string, boolean, etc. Languages have built-in data types and most languages allow you to derive new types from those built-in types like arrays, lists, stacks, etc.

Data structures are data types that are built for organizing groups of data. Mos data structures have built-in operations for traversing, searching, sorting, merging and insertion/deletion.
