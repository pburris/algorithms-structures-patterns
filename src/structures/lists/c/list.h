#ifndef H_LIST
#define H_LIST

#define LIST_CAPACITY 64

struct node {
    int value;
    struct node* next;
};
typedef struct node node_t;

struct list {
    int     length;
    node_t* head;
};
typedef struct list list_t;

void list_print(list_t *lst);
node_t *list_create_node(int value);
list_t *list_new();
void list_append(list_t *lst, int value);

node_t *list_get(list_t *lst, int value);

#endif