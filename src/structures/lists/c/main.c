#include <stdio.h>
#include <stdlib.h>
#include "./list.h" 

int main() {
    list_t *lst = list_new();
    for (int i = 0; i < 25; i++) {
        list_append(lst, i);
    }
    list_print(lst);

    node_t *tmp = list_get(lst, 13);
    if (tmp) printf("Node value: %d\n", tmp->value);

    return 0;
}

void list_print(list_t *lst) {
    node_t *temp = lst->head;
    while (temp != NULL) {
        printf("%d - ", temp->value);
        temp = temp->next;
    }
    printf("\n");
}

node_t *list_create_node(int value) {
    node_t *n;
    n = malloc(sizeof(node_t));
    n->value = value;
    n->next = NULL;
    return n;
};

list_t *list_new() {
    list_t *l = malloc(sizeof(list_t));
    l->head = NULL;
    l->length = 0;
    return l;
}

void list_append(list_t *lst, int value) {
    node_t *last = lst->head;
    if (last == NULL) {
        lst->head = list_create_node(value);
        lst->length++;
    } else {
        while (last->next != NULL) {
            last = last->next;
        }
        last->next = list_create_node(value);
        lst->length++;
    }
}

node_t *list_get(list_t *lst, int value) {
    node_t *tmp = lst->head;
    while (tmp != NULL) {
        if (tmp->value == value) return tmp;
        tmp = tmp->next;
    }
    return NULL;
};