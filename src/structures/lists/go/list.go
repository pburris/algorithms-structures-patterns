package list



type Node struct {
  value []byte
  index int
  prev int
  next int
}


type List interface {
  push(interface{}) []Node
  pop() Node
  shift(interface{}) []Node
  unshift() Node
  length() int
}
