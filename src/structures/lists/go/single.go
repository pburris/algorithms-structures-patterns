package list



type SinglyLinkedList struct {
  nodes []Node
}


func(s *SinglyLinkedList) push(item interface{}) []Node {
  newIndex := len(s.nodes) + 1
  byteVal, _ := item.([]byte)
  newNode := &Node{ value: byteVal, index: newIndex }
  s.nodes = append(s.nodes, *newNode)
  return s.nodes
}

func(s *SinglyLinkedList) pop() Node {
  old := s.nodes[len(s.nodes) - 1:][0]
  s.nodes = s.nodes[:len(s.nodes) - 1]
  return old
}

func(s *SinglyLinkedList) shift(item interface{}) []Node {
  newIndex := 0
  byteVal, _ := item.([]byte)
  newNode := &Node{ value: byteVal, index: newIndex }
  s.nodes = append([]Node{*newNode}, s.nodes...)
  return s.nodes
}

func(s *SinglyLinkedList) unshift() Node {
  old := s.nodes[:1][0]
  s.nodes = s.nodes[1:]
  return old
}

func(s *SinglyLinkedList) length() int {
  return len(s.nodes)
}


