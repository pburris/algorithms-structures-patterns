package list

import (
  "testing"
)

func Test_Singly_Push(t *testing.T) {
  singly := &SinglyLinkedList{nodes: []Node{} }
  singly.push("string")
  singly.push(7)
  if singly.length() != 2 {
    t.Error("Singly linked list cannot push properly")
  } else {
    t.Log("Singly linked list push passed")
  }
}

func Test_Singly_Is_List(t *testing.T) {
  singly := &SinglyLinkedList{nodes: []Node{} }
  singly.push("string")
  singly.push(7)
  list := List(singly)
  if list.length() != 2 {
    t.Error("Singly linked list tests do not pass ")
  } else {
    t.Log("Singly linked list tests passed")
  }
}
