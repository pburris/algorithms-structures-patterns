from Singly import Node


def reverse(head):
    current = head
    prevnode = None
    nextnode = None

    while current:
        nextnode = current.nextnode
        current.nextnode = prevnode
        prevnode = current
        current = nextnode
    return prevnode


a = Node(1)
b = Node(2)
c = Node(3)

a.nextnode = b
b.nextnode = c

for i in [a,b,c]:
    print(i.nextnode)

reverse(a)

for i in [a,b,c]:
    print(i.nextnode)
