
class Node(object):

    def __init__(self, value):
        self.value = value
        self.nextnode = None

    def concat(self, node):
        self.nextnode = node
        return self
