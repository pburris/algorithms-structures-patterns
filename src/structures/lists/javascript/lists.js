/**
 * List classes for JavaScript Lists
 */



// Node in a List
export class Node {
  constructor(value, index, prev = null, next = null) {
    this.index = index;
    this.value = value;
    this.prev = prev;
    this.next = next;
  }
}


// Basic List
export class List {
  constructor(value) {
    if (instanceof value === Array) {
      value = value.map((val, i) => this.makeNode(val, i));
    } else {
      value = [this.makeNode(value, 0)];
    }
    this.nodes = value;
  }

  // Make a new node
  makeNode = (val, index) => new Node(val, index)

  // Return the length of the list
  length() {
    return this.nodes.length;
  }

  // Re index the list
  reIndex = () =>
    this.nodes.map((node, i) => {
      let newNode = this.makeNode(node.value, i, node.prev, node.next);
      return newNode;
    });

  // Push item to the end of the list
  push(item) {
    let index = this.length() + 1;
    let newNode = this.makeNode(item, index);
    this.nodes = this.nodes.concat(newNode);
    this.nodes = this.reIndex();
    return this.nodes;
  }

  // Pop item off of the end of the list
  pop() {
    let item = this.nodes.pop();
    return item;
  }

  // Unshift item to the start of the list
  unshift(item) {
    let index = 0;
    let newNode = this.makeNode(item, index);
    this.nodes = this.nodes.unshift(newNode);
    this.nodes = this.reIndex();
    return this.nodes;
  }

  // Shift item off of the front of the list
  shift() {
    let item = this.nodes.shift();
    this.nodes = this.reIndex();
    return item;
  }
}


// Linked List
export class LinkedList extends List {
  constructor(value) {
    super(value);
    this.nodes = this.relink();
  }

  // Re link the items
  relink = () =>
    this.nodes.map((node, i) => {
      if (i > 0) node.prev = this.nodes[i - 1];
      if ((i + 1) < this.nodes.length) node.next = this.nodes[i + 1];
      return node;
    });

  // Override push method from List to run relink method
  push(item) {
    super(item);
    this.nodes = this.relink();
    return this.nodes;
  }

  pop() {
    let item = super();
    this.nodes = this.relink();
    return item;
  }

  unshift(item) {
    super(item);
    this.nodes = this.relink();
    return this.nodes;
  }

  shift() {
    let item = super();
    this.nodes = this.relink();
    return item;
  }
}
